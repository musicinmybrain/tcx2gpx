Welcome to tcx2gpx documentation!
=================================

.. toctree::
   :maxdepth: 1
   :caption: Getting Started

   introduction
   installation
   usage

.. toctree::
   :maxdepth: 2
   :caption: API

   tcxgpx
