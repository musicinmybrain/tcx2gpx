"""
Setup tcx2gpx for package building
"""
import sys
import setuptools
import versioneer

sys.path.insert(0, ".")
setuptools.setup(
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
)
